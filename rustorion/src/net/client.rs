use std::io;
use std::sync::Arc;
use std::net::ToSocketAddrs;
use tokio::net::TcpStream;
use tokio_rustls::{ TlsConnector, rustls::ClientConfig, webpki::DNSNameRef };
use anyhow::{anyhow, Result, Context, bail};
use tokio_util::codec;
use tokio::stream::StreamExt;
use futures::SinkExt;
use std::collections::hash_map;

pub struct Client {
	pub reader: codec::FramedRead<tokio::io::ReadHalf<tokio_rustls::client::TlsStream<TcpStream>>, codec::LengthDelimitedCodec>,
	pub writer: codec::FramedWrite<tokio::io::WriteHalf< tokio_rustls::client::TlsStream<TcpStream>>, codec::LengthDelimitedCodec>,
	pub client_config: ClientConfig,
}

pub struct AuthData {
	pub cert: Vec<u8>,
	pub key: Vec<u8>,
}

impl AuthData {
	pub fn read_or_generate(cert_path: &str, key_path: &str) -> Result<AuthData> {
		let cert_path = std::path::Path::new(cert_path);
		let key_path = std::path::Path::new(key_path);
		let (cert, key) = match std::fs::read(&cert_path).and_then(|x| Ok((x, std::fs::read(&key_path)?))) {
			Ok(x) => x,
			Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
				let mut params = rcgen::CertificateParams::new(vec!["localhost".into()]);
				params.alg = &rcgen::PKCS_ED25519;
				let cert = rcgen::Certificate::from_params(params).unwrap();
				let key = cert.serialize_private_key_der();
				let cert = cert.serialize_der().unwrap();
				std::fs::write(&cert_path, &cert).context("failed to write certificate")?;
				std::fs::write(&key_path, &key).context("failed to write private key")?;
				(cert, key)
			}
			Err(e) => {
				bail!("failed to read certificate: {}", e);
			}
		};
		Ok(AuthData {cert, key})
	}
}

impl Client {
	pub async fn connect(host: &str, auth_data: &AuthData) -> Result<Self> {
		let addr = host
			.to_socket_addrs()?
			.next()
			.ok_or_else(|| io::Error::from(io::ErrorKind::NotFound))?;
		let mut client_config = ClientConfig::new();
		client_config.dangerous().set_certificate_verifier(Arc::new(NoVerifier{}));
		client_config.set_single_client_cert(vec![rustls::Certificate(auth_data.cert.clone())], rustls::PrivateKey(auth_data.key.clone()))?;
		let connector = TlsConnector::from(Arc::new(client_config.clone()));
		
		let tcp_stream = TcpStream::connect(&addr).await?;
		let domain = DNSNameRef::try_from_ascii_str(&"localhost").context("Failed DNS")?;
		let stream = connector.connect(domain, tcp_stream).await?;
		
		let (recv, send) = tokio::io::split(stream);
	
		let reader = codec::FramedRead::new(recv, codec::LengthDelimitedCodec::new());
		let writer = codec::FramedWrite::new(send, codec::LengthDelimitedCodec::new());
		
		Ok(Client { reader, writer, client_config })
	}
	
	pub async fn get_view(&mut self) -> Result<crate::universeview::Universe> {
		let mcall = rmp_serde::encode::to_vec(&cborpc::MethodCall{ protocol_name: "rustorion-server-0".to_string(), method_name: "get_view".to_string(), message: vec![]}).unwrap().into();
		self.writer.send(mcall)
			.await
			.map_err(|e| anyhow!("failed to send request: {}", e))?;
		let response_bytes = self.reader.next().await.ok_or(anyhow!("Unexpected end of framed stream"))??; 
		
		let cr: cborpc::CallResponse = rmp_serde::decode::from_read(response_bytes.as_ref())?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}
		
		let view = rmp_serde::decode::from_read(cr.message.as_slice())?;
		
		Ok(view)
	}
	
	pub async fn get_empire_id(&mut self) -> Result<crate::storage::ID<crate::universeview::Empire>> {
		let mcall = rmp_serde::encode::to_vec(&cborpc::MethodCall{ protocol_name: "rustorion-server-0".to_string(), method_name: "get_empire_id".to_string(), message: vec![]}).unwrap().into();
		self.writer.send(mcall)
			.await
			.map_err(|e| anyhow!("failed to send request: {}", e))?;
		let response_bytes = self.reader.next().await.ok_or(anyhow!("Unexpected end of framed stream"))??;
		let cr: cborpc::CallResponse = rmp_serde::decode::from_read(response_bytes.as_ref())?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}
		
		let empire_id = rmp_serde::decode::from_read(cr.message.as_slice())?;
		
		Ok(empire_id)
	}
	
	pub async fn set_actions(&mut self, actions: &Vec<crate::universe::action::Action>) -> Result<()> {
		let actions_vec = rmp_serde::encode::to_vec(&actions)?;
		let mcall = rmp_serde::encode::to_vec(&cborpc::MethodCall{ protocol_name: "rustorion-server-0".to_string(), method_name: "set_actions".to_string(), message: actions_vec})?.into();
		self.writer.send(mcall)
			.await
			.map_err(|e| anyhow!("failed to send request: {}", e))?;
		let response_bytes = self.reader.next().await.ok_or(anyhow!("Unexpected end of framed stream"))??;
		let cr: cborpc::CallResponse = rmp_serde::decode::from_read(response_bytes.as_ref())?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}
		
		Ok(())
	}
	
	fn turn_changed(client_callbacks: &mut impl ClientCallbacks, message: &cborpc::Data) -> Result<cborpc::CallResponse> {
		let turn_number: u16 = rmp_serde::decode::from_read(message.as_slice())?;
		client_callbacks.turn_changed(turn_number)?;
		Ok(cborpc::CallResponse{success: true, message: vec![]})
	}
	
	/// Provide rustorion-client-0 interface to server
	pub async fn back_connect<CC: ClientCallbacks>(client_config: ClientConfig, host: String, client_callbacks: CC) -> Result<()> {
		let addr = host
			.to_socket_addrs()?
			.next()
			.ok_or_else(|| io::Error::from(io::ErrorKind::NotFound))?;
			
		let connector = TlsConnector::from(Arc::new(client_config.clone()));
		
		let tcp_stream = TcpStream::connect(&addr).await?;
		let domain = DNSNameRef::try_from_ascii_str(&"localhost").context("Failed DNS")?;
		let stream = connector.connect(domain, tcp_stream).await?;
		
		let (recv, send) = tokio::io::split(stream);
	
		let mut reader = codec::FramedRead::new(recv, codec::LengthDelimitedCodec::new());
		let mut writer = codec::FramedWrite::new(send, codec::LengthDelimitedCodec::new());
		
		let mut responder = cborpc::Responder::new(client_callbacks);
		let protocols = responder.protocols();
		let mut server_protocol = hash_map::HashMap::new();
		server_protocol.insert("turn_changed".to_string(), Self::turn_changed as cborpc::CallHandler<CC>);
		protocols.insert("rustorion-client-0".to_string(), server_protocol);
		
		let mut next_opt = reader.next().await;
		while next_opt.is_some() {
			let next = next_opt.ok_or(anyhow!("This will never happen"))??;
			let inpacket: &[u8] = next.as_ref();
			let mut outpacket = vec![];
			let success: bool = responder.answer_call(inpacket, &mut outpacket).unwrap();
			
			writer.send(outpacket.into()).await?;
			eprintln!("Call answered success={}", success);
			next_opt = reader.next().await;
		}
		Ok(())
	}
}

pub trait ClientCallbacks {
	fn turn_changed(&mut self, new_turn: u16) -> Result<()>;
}


struct NoVerifier;

impl rustls::ServerCertVerifier for NoVerifier {
	fn verify_server_cert(
		&self,
		_roots: &rustls::RootCertStore,
		_presented_certs: &[rustls::Certificate],
		_dns_name: DNSNameRef,
		_ocsp_response: &[u8]
	) -> std::result::Result<rustls::ServerCertVerified, rustls::TLSError> {
		Ok(rustls::ServerCertVerified::assertion())
	}
}

