// frontend for manipulation of Universe data
// Encapsulates IDs and borrows universe data to ensure type safety.

use crate::universe;
use crate::storage::EntityStored;

#[derive(Copy, Clone)]
pub struct StarSystem<'a> {
	pub data: &'a universe::StarSystem,
	pub universe: &'a universe::Universe,
}

impl<'a> StarSystem<'a> {
	pub fn empire(&self) -> Option<Empire> {
		self.universe.star_systems_in_empires.parent(self.data.id).map(|e_id| Empire { data: self.universe.getf(e_id).unwrap(), universe: self.universe })
	}
}

#[derive(Copy, Clone)]
pub struct Empire<'a> {
	pub data: &'a universe::Empire,
	pub universe: &'a universe::Universe,
}

impl<'a> Empire<'a> {
	pub fn star_systems(&self) -> Vec<StarSystem> {
		self.universe.star_systems_in_empires.linked(self.data.id).into_iter().map(|id| StarSystem { universe: self.universe, data: self.universe.getf(id).unwrap() }).collect()
	}
	
	pub fn capital(&self) -> Option<StarSystem> {
		self.universe.capitals_in_empires.child(self.data.id).map(|id| StarSystem { universe: self.universe, data: self.universe.getf(id).unwrap() })
	}
}

#[derive(Copy, Clone)]
pub struct Universe<'a> {
	pub data: &'a universe::Universe,
}

impl<'a> Universe<'a> {
	pub fn empires(&self) -> Vec<Empire> {
		self.data.empires.iter().map(|x| Empire { universe: self.data, data: x.1 }).collect()
	}
}
