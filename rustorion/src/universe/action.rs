use crate::universe as u;
use crate::storage as s;
use std::collections::{hash_set::HashSet};
use crate::storage::{
	ID,
	IDentified,
	EntityStored,
};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub enum Action {
	CaptureStarSystem{star_system: s::ID<u::StarSystem>, empire: s::ID<u::Empire>},
	MoveShip{ship: s::ID<u::Ship>, to: s::ID<u::StarSystem>},
}

#[derive(Serialize, Deserialize)]
pub struct ActionFailure {
	pub action: Action,
	pub error: String,
}

#[derive(Serialize, Deserialize)]
pub struct Actor {
	pub empire_id: s::ID<u::Empire>
}

/// Holds information that is required to detect collisions in user-submitted actions
#[derive(Default)]
pub struct ActionCache {
	pub star_systems_being_captured: HashSet<ID<u::StarSystem>>,
}


/// Represents the action player decides to take, like capturing a planet.
/// Actions should be able to be verified to be sane by client, if server receives them and they fail to validate, it is a general error.
/// When all players have submitted their actions, they are applied (this cannot fail, just can produce different results) and turn into various pending situations which are handled by universe updating routine.
impl Action {
	pub fn validate_batch(actions: &[Action], universe: &u::Universe, actor: &Actor) -> Vec<ActionFailure> {
		let mut failures = vec![];
		let mut action_cache = ActionCache::default();
		for action in actions {
			action.validate(&mut action_cache, universe, actor).map_err(|et| failures.push(ActionFailure{ action: action.clone(), error: et.to_string() })).unwrap_or(());
		}
		failures
	}
	pub fn validate(&self, action_cache: &mut ActionCache, universe: &u::Universe, actor: &Actor) -> Result<(), &'static str> {
		match self {
			Action::CaptureStarSystem{star_system: ss_id, empire: e_id} => {
				Self::check_id(*ss_id, universe)?;
				Self::check_id(*e_id, universe)?;
				// Can only capture systems for yourself
				if *e_id != actor.empire_id {
					return Err("Capturer is not the actor");
				}
				
				if universe.star_systems_in_empires.parent(*ss_id) == Some(actor.empire_id) {
					return Err("Can't capture own systems");
				}
				if !action_cache.star_systems_being_captured.insert(*ss_id) {
					return Err("Already being captured");
				}
				let ships = universe.ships_in_star_systems.linked(*ss_id);
				let mut has_owned_ships = false;
				for ship in ships {
					let empire = universe.ships_in_empires.parent(ship);
					if empire != Some(actor.empire_id) {
						return Err("Others' ships are present, cannot capture the system");
					} else {
						has_owned_ships = true;
					}
				}
				if !has_owned_ships {
					return Err("Cannot capture a star system w/o any warships");
				}
			}
			Action::MoveShip{ship, to} => {
				Self::check_id(*ship, universe)?;
				Self::check_id(*to, universe)?;
				if universe.ships_in_empires.parent(*ship) != Some(actor.empire_id) {
					return Err("Can't move others' ships");
				}
				// check if the target system is adjacent to the source system
				let current_ss = universe.ships_in_star_systems.parent(*ship).unwrap();
				if !universe.starlanes.linked(current_ss, *to){
					return Err("Can't move ships between non-linked star systems");
				}
			}
		}
		Ok(())
	}
	
	pub fn apply(&self, universe: &mut u::Universe) {
		match self {
			Action::CaptureStarSystem{star_system: ss_id, empire: e_id} => {
				if let Some(current_owner) = universe.star_systems_in_empires.parent(*ss_id) {
					universe.star_systems_in_empires.del_link(current_owner, *ss_id);
				}
				universe.set_star_system_owner(*ss_id, *e_id);
			}
			Action::MoveShip{ship, to} => {
				let current_ss = universe.ships_in_star_systems.parent(*ship).unwrap();
				universe.ships_in_star_systems.del_link(current_ss, *ship);
				universe.ships_in_star_systems.add_link(*to, *ship);
			}
		}
	}
	
	fn check_id<Entity: IDentified>(id: ID<Entity>, storage: &impl EntityStored<Entity>) -> Result<(), &'static str> {
		if storage.exists(id) {
			Ok(())
		} else {
			Err("ID not found in storage")
		}
	}
}
