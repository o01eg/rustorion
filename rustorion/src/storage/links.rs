use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::collections::HashSet;
use crate::storage::{
	ID,
};

/* Many-to-many relationship between entities */
#[derive(Serialize, Deserialize)]
#[serde(transparent)]
pub struct Interlink<Entity> {
	pub map: HashMap<ID<Entity>, HashSet<ID<Entity>>>,
}

impl<Entity> Default for Interlink<Entity> {
	fn default() -> Self {
		Self { map: HashMap::new() }
	}
}

impl<Entity> Interlink<Entity> {
	// Add a link between two entities if not present
	pub fn add_link(&mut self, entity1_id: ID<Entity>, entity2_id: ID<Entity>) -> bool {
		let child_set = self.map.entry(entity1_id).or_default();
		if child_set.contains(&entity2_id) {
			return false;
		} else {
			child_set.insert(entity2_id);
		}
		self.map.entry(entity2_id).or_default().insert(entity1_id);
		return true;
	}
	
	// List all links of an entity
	pub fn links(&self, entity_id: ID<Entity>) -> Vec<ID<Entity>> {
		self.map.get(&entity_id).map_or_else(|| vec![], |hset| hset.into_iter().cloned().collect())
	}
	
	// Check if two entities are linked
	pub fn linked(&self, entity1_id: ID<Entity>, entity2_id: ID<Entity>) -> bool {
		match self.map.get(&entity1_id) {
			Some(hm) => hm.contains(&entity2_id),
			None => false
		}
	}
}

// One-to-many link
#[derive(Serialize, Deserialize)]
pub struct HasMany<Parent, Child> {
	pub children_mmap: HashMap<ID<Parent>, HashSet<ID<Child>>>,
	pub parent_map: HashMap<ID<Child>, ID<Parent>>,
}

impl<Parent,Child> Default for HasMany<Parent, Child> {
	fn default() -> Self {
		Self {
			children_mmap: HashMap::new(),
			parent_map: HashMap::new(),
		}
	}
}

impl<Parent,Child> HasMany<Parent,Child> {
	// Add a link between parent and child
	pub fn add_link(&mut self, p_id: ID<Parent>, c_id: ID<Child>) -> bool {
		let child_set = self.children_mmap.entry(p_id).or_default();
		if child_set.contains(&c_id) {
			return false;
		} else {
			child_set.insert(c_id);
		}
			
		self.parent_map.insert(c_id, p_id);
		return true;
	}
	
	// Delete link between parent and child
	pub fn del_link(&mut self, p_id: ID<Parent>, c_id: ID<Child>) -> bool {
		match self.children_mmap.get_mut(&p_id) {
			Some(children_set) => {
				children_set.remove(&c_id);
			},
			None => return false,
		}
		
		self.parent_map.remove(&c_id);
		return true;
	}

    // Remove links concerning a parent
	pub fn purge_parent(&mut self, p_id: ID<Parent>) -> bool {
		for child in self.linked(p_id) {
    		self.del_link(p_id, child);
        };
        true
	}
	

	// Remove links concerning a child
	pub fn purge_child(&mut self, c_id: ID<Child>) -> bool {
		let p_id = match self.parent(c_id) {
			None => return false,
			Some(p_id) => p_id,
		};
		self.del_link(p_id, c_id)
	}
	
	// List all children of a parent
	pub fn linked(&self, p_id: ID<Parent>) -> Vec<ID<Child>> {
		self.children_mmap.get(&p_id).map_or_else(|| vec![], |hset| hset.iter().map(|x| *x).collect())
	}
	
	// Check if parent is linked to a child
	pub fn link_present(&self, p_id: ID<Parent>, c_id: ID<Child>) -> bool {
		match self.children_mmap.get(&p_id) {
			Some(hset) => hset.contains(&c_id),
			None => return false,
		}
	}
	
	// Get child's parent
	pub fn parent(&self, c_id: ID<Child>) -> Option<ID<Parent>> {
		self.parent_map.get(&c_id).map(|x| *x)
	}

}

// One-to-one link, one parent can be linked to one child, and vice-versa
#[derive(Serialize, Deserialize)]
#[serde(transparent)]
pub struct HasOne<Parent, Child> {
	pub bimap: bimap::BiMap<ID<Parent>, ID<Child>>,
}

impl<Parent,Child> Default for HasOne<Parent, Child> {
	fn default() -> Self {
		Self {
			bimap: bimap::BiMap::new(),
		}
	}
}

impl <Parent, Child> HasOne<Parent, Child> {
	// Add a link between parent and child
	pub fn add_link(&mut self, p_id: ID<Parent>, c_id: ID<Child>) -> bool {
		self.bimap.insert_no_overwrite(p_id, c_id).map(|_| true).unwrap_or(false)
	}
	
	// Delete link between parent and child
	pub fn del_by_parent(&mut self, p_id: ID<Parent>) -> bool {
		self.bimap.remove_by_left(&p_id).map(|_| true).unwrap_or(false)
	}
	
	// Delete link between child and parent
	pub fn del_by_child(&mut self, p_id: ID<Child>) -> bool {
		self.bimap.remove_by_right(&p_id).map(|_| true).unwrap_or(false)
	}
	
	
	// Check if parent is linked to a child
	pub fn link_present(&self, p_id: ID<Parent>, c_id: ID<Child>) -> bool {
		match self.bimap.get_by_left(&p_id) {
			Some(c_id_there) if *c_id_there == c_id => true,
			_ => false,
		}
	}
	
	// Get child's parent
	pub fn parent(&self, c_id: ID<Child>) -> Option<ID<Parent>> {
		self.bimap.get_by_right(&c_id).map(|x| *x)
	}
	
	// Get child's parent
	pub fn child(&self, c_id: ID<Parent>) -> Option<ID<Child>> {
		self.bimap.get_by_left(&c_id).map(|x| *x)
	}
}
