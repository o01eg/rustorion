

use crate::storage;

#[derive(Fail, Debug)]
#[fail(display = "Entity ID={} was not present in storage", id)]
pub struct EntityMissing {
	id: storage::StorageID,
}
