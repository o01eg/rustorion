// Name generation
// language based on finnish phonetics, with enforced order of vowels and consonants
// only easy-to-pronounce (subjective) doubles are permitted

extern crate rand;

use ::rand::prelude::SliceRandom;

const VOWELS: [&'static str; 5] = ["a", "e", "u", "i", "o"];
const DIVOWELS: [&'static str; 25] = ["aa", "ae", "au", "ai", "ao", "ea", "ee", "eu", "ei", "eo", "ua", "ue", "uu", "ui", "uo", "ia", "ie", "iu", "ii", "io", "oa", "oe", "ou", "oi", "oo"];
const CONSONANTS: [&'static str; 15] = ["r", "t", "p", "s", "d", "f", "g", "h", "k", "l", "z", "v", "b", "n", "m"];
const DICONSONANTS: [&'static str; 143] = ["rr", "rt", "rp", "rs", "rd", "rf", "rg", "rh", "rk", "rl", "rz", "rv", "rb", "rn", "rm", "tt", "ts", "tk", "tl", "tz", "pt", "ps", "pk", "pl", "sr", "st", "sp", "ss", "sf", "sk", "sl", "sn", "sm", "dr", "ds", "dd", "df", "dg", "dh", "dl", "dz", "dv", "fr", "ft", "fs", "ff", "fk", "fl", "fn", "fm", "gr", "gs", "gf", "gl", "gz", "gn", "ht", "hp", "hs", "hf", "hg", "hh", "hk", "hl", "hz", "hv", "hn", "hm", "kr", "kt", "ks", "kf", "kh", "kk", "kl", "kz", "kv", "kn", "km", "ls", "ld", "lf", "lg", "lh", "lk", "ll", "lz", "lv", "lb", "ln", "lm", "zr", "zl", "zz", "zb", "zn", "zm", "vr", "vd", "vg", "vl", "vz", "vv", "vn", "vm", "br", "bg", "bh", "bl", "bz", "bb", "bn", "bm", "nr", "nt", "np", "ns", "nd", "nf", "ng", "nh", "nk", "nl", "nz", "nv", "nb", "nn", "nm", "mr", "mt", "mp", "ms", "md", "mf", "mg", "mh", "mk", "ml", "mz", "mv", "mb", "mn", "mm"];

// Generate a name of given length and append a suffix.
// Ensures suffix does not break vowel harmony by appending an extra letter if necessary
pub fn generate_name(rng: &mut impl rand::Rng, length: usize, suffix: &str) -> String {
	let mut word = String::new();
	let mut vowel = rng.gen::<bool>();
	fn append_part(rng: &mut impl rand::Rng, word: &mut String, vowel: &mut bool, length: &usize) {
		let double = rng.gen_range(0, 100) < 20 && word.len() != 0 && word.len() < length-1;
		if *vowel {
			let part = if double {
				DIVOWELS.choose(rng)
			} else {
				VOWELS.choose(rng)
			};
			word.push_str(part.unwrap());
		} else {
			let part = if double {
				DICONSONANTS.choose(rng)
			} else {
				CONSONANTS.choose(rng)
			};
			word.push_str(part.unwrap());
		}
	};
	while word.len() < length {
		append_part(rng, &mut word, &mut vowel, &length);
		vowel = !vowel;
	};
	if suffix.len() != 0 {
		let suffix_start = suffix.chars().nth(0);
		let vowel_next = VOWELS.iter().any(|c| { c.chars().nth(0) == suffix_start });
		let vowel_before = !vowel;
		if vowel_next == vowel_before {
			// Do another round to avoid disallowed neighbors
			append_part(rng, &mut word, &mut vowel, &length);
		};
		word.push_str(suffix);
	}
	word
}
