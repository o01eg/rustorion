// Universe generation

use crate::universe;
use crate::names;
extern crate mint;
use rand::seq::SliceRandom;
use crate::storage::EntityStored;
use anyhow::{Result, anyhow};

pub struct UniverseParameters {
	pub star_systems_n: u64,
	pub empires_n: u64,
	pub add_testables: bool,
	pub uninhabited: f64,
}

impl Default for UniverseParameters {
	fn default() -> Self {
		UniverseParameters {
			star_systems_n: 100,
			empires_n: 5,
			uninhabited: 0.2,
			add_testables: false,
		}
	}
}

pub fn find_place_for_star_system(rng: &mut impl rand::Rng, _u: &universe::Universe) -> universe::UniverseLocation {
	// TODO: RTRee
	let x = rng.gen_range(-100., 100.) * 100. + rng.gen_range(-20., 20.);
	let y = rng.gen_range(-100., 100.) * 100. + rng.gen_range(-20., 20.);
	return [x,y];
}

pub fn generate_universe(rng: &mut impl rand::Rng, params: &UniverseParameters) -> Result<universe::Universe> {
	let mut u = universe::Universe::default();
	
	for _ in 0..params.empires_n {
		let name = names::generate_name(rng, 5, "an");
		let color = [rng.gen_range(0.5, 1.), rng.gen_range(0.5, 1.), rng.gen_range(0.5, 1.)];
		u.add_empire(&name, color).unwrap();
	}
	
	for _ in 0..params.star_systems_n {
		let location = find_place_for_star_system(rng, &u);
		let population = rng.gen_range(1e5 as i64, 1e18 as i64);
		let industry = 10;
		let star_system_id = u.add_star_system(location, names::generate_name(rng, 5, "ot"), population, industry).unwrap();
		u.add_planet(star_system_id).unwrap();
		if !rng.gen_bool(params.uninhabited)
		{
			let empire_ids = u.all_ids();
			let empire_id = *empire_ids.choose(rng).unwrap();
			u.set_star_system_owner(star_system_id, empire_id);
			if u.capitals_in_empires.child(empire_id) == None {
				u.set_empire_capital(empire_id, star_system_id);
			}
		}
	}

	generate_starlanes(&mut u);
	
	if params.add_testables {
		let ui = crate::universe::interface::Universe { data: &u };
		let empires = ui.empires();
		let e1 = empires.get(0).ok_or(anyhow!("Not enough empires for test"))?;
		let e2 = empires.get(1).ok_or(anyhow!("Not enough empires for test"))?;
		let ss1_id = e1.capital().ok_or(anyhow!("Capitalless empire"))?.data.id;
		let ss2_id = e2.capital().ok_or(anyhow!("Capitalless empire"))?.data.id;
		let e1_id = e1.data.id;
		let e2_id = e2.data.id;
		u.getf_mut(e1_id)?.name = "Empire1".to_string();
		u.getf_mut(e2_id)?.name = "Empire2".to_string();
		u.getf_mut(ss1_id)?.name = "StarSystem1".to_string();
		u.getf_mut(ss2_id)?.name = "StarSystem2".to_string();
		u.starlanes.add_link(ss1_id, ss2_id);
	}
	Ok(u)
}

fn generate_starlanes(u: &mut universe::Universe) {

	let max_starlane_length = 4000.;
	for (_, system) in &u.star_systems {
		for (_, potential_system) in &u.star_systems {
			
			if system != potential_system
				&& distance(potential_system.location, system.location) < max_starlane_length {
				u.starlanes.add_link(system.id, potential_system.id);
			}
		}
	}
}

fn distance(l1: universe::UniverseLocation, l2: universe::UniverseLocation) -> f32 {
	(((l1[0] - l2[0]).powf(2.) + (l1[1] - l2[1]).powf(2.))).sqrt()
}
