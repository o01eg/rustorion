#![cfg_attr(feature = "strict", deny(warnings))]

#[macro_use] extern crate failure_derive;

pub mod names;
pub mod universe;
pub mod universegen;
pub mod storage;
pub mod universeview;
pub mod net;
