pub mod interface;
pub mod action;

// Low-level universe data storage
// Use-cases: modification of universe data (higher interfaces are read-only)
//            building high-level interfaces

use crate::storage::{
	ID,
	IDentified,
	StorageID,
	EntityStored,
	IssuesIDs
};
use crate::storage::links;
use crate::names;
use byteorder::LittleEndian;
use std::collections::HashMap;
use std::collections::HashSet;
use block_cipher_trait::BlockCipher;
use rand::Rng;
use blowfish::Blowfish;
use serde::{Deserialize, Serialize};
use anyhow::Result;

extern crate mint;

// #[derive(PartialEq, Eq, PartialOrd, Clone, Copy, Serialize, Deserialize)]
pub type UniverseLocation = [f32; 2];
pub type Color = [f32; 3];

#[derive(PartialEq, Eq, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Planet {
	pub id: ID<Planet>,
}

impl IDentified for Planet {
	fn id(&self) -> ID<Self> {
		return self.id;
	}
	
	fn id_mut(&mut self) -> &mut ID<Self> {
		return &mut self.id;
	}
}

#[derive(PartialEq, Clone, Serialize, Deserialize)]
pub struct StarSystem {
	pub id: ID<StarSystem>,
	pub name: String,
	pub location: UniverseLocation,
	pub population: i64,
	pub industry: u64,
}

impl IDentified for StarSystem {
	fn id(&self) -> ID<Self> {
		return self.id;
	}
	
	fn id_mut(&mut self) -> &mut ID<Self> {
		return &mut self.id;
	}
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct Empire {
	pub id: ID<Empire>,
	pub name: String,
	pub color: Color,
	pub stockpile_industry: u64,
	pub submitted_actions: Option<Vec<action::Action>>,
}

impl IDentified for Empire {
	fn id(&self) -> ID<Self> {
		return self.id;
	}
	
	fn id_mut(&mut self) -> &mut ID<Self> {
		return &mut self.id;
	}
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct Ship {
	pub id: ID<Ship>,
	pub name: String,
}

impl IDentified for Ship {
	fn id(&self) -> ID<Self> {
		return self.id;
	}
	
	fn id_mut(&mut self) -> &mut ID<Self> {
		return &mut self.id;
	}
}

#[derive(Serialize, Deserialize)]
pub struct Universe {
	pub planets: HashMap<StorageID, Planet>,
	pub star_systems: HashMap<StorageID, StarSystem>,
	current_id: StorageID,
	pub empires: HashMap<StorageID, Empire>,
	pub planets_in_star_systems: links::HasMany<StarSystem, Planet>,
	pub starlanes: links::Interlink<StarSystem>,
	pub star_systems_in_empires: links::HasMany<Empire, StarSystem>,
	pub ships: HashMap<StorageID, Ship>,
	pub ships_in_star_systems: links::HasMany<StarSystem, Ship>,
	pub ships_in_empires: links::HasMany<Empire, Ship>,
	pub id_storage_key: [u8; 8],
	pub turn_number: u64,
	pub capitals_in_empires: links::HasOne<Empire, StarSystem>,
}

impl Default for Universe {
	fn default() -> Self {
		let mut id_storage_key: [u8; 8] = <[u8; 8]>::default();
		rand::thread_rng().fill(&mut id_storage_key);
		Universe {
			planets: HashMap::default(),
			star_systems: HashMap::default(),
			current_id: 0,
			empires: HashMap::default(),
			ships: HashMap::default(),
			planets_in_star_systems: links::HasMany::default(),
			ships_in_star_systems: links::HasMany::default(),
			ships_in_empires: links::HasMany::default(),
			starlanes: links::Interlink::default(),
			star_systems_in_empires: links::HasMany::default(),
			capitals_in_empires: links::HasOne::default(),
			id_storage_key,
			turn_number: 0,
		}
	}
}

impl EntityStored<Planet> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Planet> {
		&self.planets
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Planet> {
		&mut self.planets
	}
}

impl EntityStored<StarSystem> for Universe {
	fn storage(&self) -> &HashMap<StorageID, StarSystem> {
		&self.star_systems
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, StarSystem> {
		&mut self.star_systems
	}
}

impl EntityStored<Ship> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Ship> {
		&self.ships
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Ship> {
		&mut self.ships
	}
	fn unindex(&mut self, ship_id: ID<Ship>) -> Result<()> {
		self.ships_in_star_systems.purge_child(ship_id);
		self.ships_in_empires.purge_child(ship_id);
		Ok(())
	}
}

impl EntityStored<Empire> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Empire> {
		&self.empires
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Empire> {
		&mut self.empires
	}
}

impl IssuesIDs for Universe {
	fn next_id(&mut self) -> StorageID {
		self.current_id += 1;
		let mut bytes = self.current_id.to_le_bytes().into();
		let blowfish = Blowfish::<LittleEndian>::new_varkey(&self.id_storage_key).unwrap();
		blowfish.encrypt_block(&mut bytes);
		u64::from_le_bytes(bytes.into())
	}
}

impl Universe {
	pub fn add_star_system(&mut self, location: UniverseLocation, name: String, population: i64, industry: u64) -> Result<ID<StarSystem>> {
		let star_system = StarSystem {
			id: ID::invalid(),
			name,
			location,
			population,
			industry,
		};
		self.insert(star_system)
	}
	
	pub fn add_empire(&mut self, name: &str, color: Color) -> Result<ID<Empire>> {
		self.insert(Empire {
			id: ID::invalid(),
			color,
			name: String::from(name),
			submitted_actions: None,
			stockpile_industry: 0,
		})
	}
	
	pub fn add_planet(&mut self, star_system_id: ID<StarSystem>) -> Result<ID<Planet>> {
		let planet_id = self.insert(Planet {id: ID::invalid()})?;
		self.planets_in_star_systems.add_link(star_system_id, planet_id);
		Ok(planet_id)
	}
	
	pub fn set_star_system_owner(&mut self, star_system_id: ID<StarSystem>, owner_id: ID<Empire>) {
		self.star_systems_in_empires.add_link(owner_id, star_system_id);
	}
	
	pub fn add_ship(&mut self, name: &str, star_system_id: ID<StarSystem>, owner_id: ID<Empire>) -> Result<ID<Ship>> {
		let ship_id = self.insert(Ship {
			id: ID::invalid(),
			name: String::from(name),
		})?;
		self.ships_in_star_systems.add_link(star_system_id, ship_id);
		self.ships_in_empires.add_link(owner_id, ship_id);
		Ok(ship_id)
	}
	
	pub fn apply_actions(&mut self) {
		let empires = self.empires.iter_mut().map(|x|x.1);
		let actions: Vec<_> = empires.map(|e|{let a = e.submitted_actions.clone().unwrap_or(vec![]); e.submitted_actions = None; a}).collect::<Vec<Vec<action::Action>>>().as_slice().concat();
		for action in actions {
			action.apply(self);
		}
	}
	
	pub fn make_turn(&mut self) -> Result<()> {
		self.build_ships()?;
		self.apply_actions();
		self.resolve_battles();
		self.gather_industry();
		self.turn_number += 1;
		Ok(())
	}

	pub fn gather_industry(&mut self) {
		let ui = crate::universe::interface::Universe { data: &self };
		let mut empires_industry = Vec::with_capacity(ui.empires().len());
		for empire in ui.empires() {
			let mut ip = 0;
			for star_systems in empire.star_systems(){
				ip += star_systems.data.industry;
			}
			empires_industry.push((empire.data.id, ip));
		}
		for (id, ip) in empires_industry {
			self.getf_mut(id).unwrap().stockpile_industry += ip;
		}
	}
	
	pub fn set_empire_capital(&mut self, e_id: ID<Empire>, ss_id: ID<StarSystem>) {
		if let Some(current_capital_id) = self.capitals_in_empires.child(e_id) {
			self.getf_mut(current_capital_id).unwrap().industry -= 10;
		}
		self.capitals_in_empires.add_link(e_id, ss_id);
		self.getf_mut(ss_id).unwrap().industry += 10;
	}
	
	pub fn build_ships(&mut self) -> Result<()> {
		for (empire, capital_option) in self.empires.iter().map(|x|(x.1.id(), self.capitals_in_empires.child(x.1.id()))).collect::<Vec<_>>() {
			if let Some(capital) = capital_option {
				while self.getf(empire).unwrap().stockpile_industry >= 100{
					let name = names::generate_name(&mut rand::thread_rng(), 5, "en");
					self.add_ship(&name, capital, empire)?;
					self.getf_mut(empire).unwrap().stockpile_industry -= 100;
				}
			}
		}
		Ok(())
	}
	pub fn resolve_battles(&mut self) {
		let mut dead_ships = HashSet::with_capacity(self.ships.len());
		let star_system_ids = self.star_systems.values().map(|x|x.id());
		// FIXME persistent RNG
		let mut rng = rand::thread_rng();
		for ssid in star_system_ids {
			let ships = self.ships_in_star_systems.linked(ssid);
			
			let mut empires_ships : HashMap<ID<Empire>, u32> = HashMap::new();
			for ship in &ships {
				let empire = self.ships_in_empires.parent(*ship).unwrap();
				empires_ships.entry(empire)
					.and_modify(|e| { *e += 1 })
					.or_insert(1);
			}
			for empire in empires_ships.keys() {
				
				// FIXME this should be copy-on-filter instead of two passes
				let mut enemy_ships = ships.clone();
				// a valid target is not our own ship and still exists
				enemy_ships.retain(|ship| self.ships_in_empires.parent(*ship) != Some(*empire) && !dead_ships.contains(ship));
				if enemy_ships.len() > 0 {
					let our_ships: u32 = empires_ships.get(empire).cloned().unwrap_or(0);
					for _ in 0..our_ships {
						// if there is a ship left
						if enemy_ships.len() == 0 { break };
						// our ship shoots at enemies!
						let which_enemy : u32 = rng.gen_range(0, enemy_ships.len() as u32);
						// enemy ded, queue for removal
						dead_ships.insert(enemy_ships[which_enemy as usize]);
						// and don't allow further shoots at it
						enemy_ships.remove(which_enemy as usize);
					}
				}
			}
		}
		for ship in dead_ships {
			self.remove(ship).unwrap();
		}
	}
}

// pub struct UniqueOrderedIndex<Key, Entity> {
// 	map: BTreeMap<Key, ID<Entity>>,
// }
// 
// impl<Key, Entity> UniqueOrderedIndex<Key, Entity> {
// 	pub fn add_index(&mut self, key: Key, entity_id: ID<Entity>) {
// 		self.map.insert()
// 	}  
// }

#[cfg(test)]
mod tests {
	// Note this useful idiom: importing names from outer (for mod tests) scope.
// 	use super::*;
	use crate::storage::EntityStored;
	/// Two empires engage in a reckless assault, strategic acquirement and victorious counteroffensive.
	#[test]
	fn test_galactic_war() {
		let mut u = super::Universe::default();
		
		let e1 = u.add_empire("Empire1", [0.,0.,0.]).unwrap();
		let e2 = u.add_empire("Empire2", [1.,1.,1.]).unwrap();
		
		
		// add some disjoint star system for the industrial base
		for i in 0..10 {
			let ss_id = u.add_star_system([100.0+i as f32, 100.0+i as f32], format!("StarSystem1.{}", i), 1, 10).unwrap();
			if i == 0 {
				u.set_empire_capital(e1, ss_id);
			}
			u.star_systems_in_empires.add_link(e1, ss_id);
		}
		
		for i in 0..10 {
			let ss_id = u.add_star_system([i as f32, i as f32], format!("StarSystem2.{}", i), 1, 10).unwrap();
			if i == 0 {
				u.set_empire_capital(e2, ss_id);
			}
			u.star_systems_in_empires.add_link(e2, ss_id);
		}
		
		// connect the capitals for the assault
		
		let capital1 = u.star_systems.iter().find(|(_, ss)| ss.name == "StarSystem1.0").unwrap().1.id;
		let capital2 = u.star_systems.iter().find(|(_, ss)| ss.name == "StarSystem2.0").unwrap().1.id;
		u.starlanes.add_link(capital1, capital2);
		
		// Advance turns several times to gain fleets
		for _ in 0..10 {
			u.make_turn().unwrap();
		}
		
		assert_eq!(9, u.ships_in_star_systems.linked(capital1).len());
		assert_eq!(9, u.ships_in_star_systems.linked(capital2).len());
		
		// Attack capital 2 with fleet of empire 1
		
		let ships = u.ships_in_star_systems.linked(capital1);
		u.getf_mut(e1).unwrap().submitted_actions = Some(ships.into_iter().map(|ship_id| crate::universe::action::Action::MoveShip{ship: ship_id, to: capital2}).collect());
		
		u.make_turn().unwrap();
		
		// Should only have survivor ships from empire 2 there now
		let ships_owners: Vec<_> = u.ships_in_star_systems.linked(capital2).into_iter().map(|s| u.ships_in_empires.parent(s).unwrap()).collect();
 		assert_eq!(ships_owners.as_slice(), &[e2, e2]);
 		
 		// Empire 2 discovers routes to some empire 1 system
 		
 		let ss1 = u.star_systems.iter().find(|(_, ss)| ss.name == "StarSystem1.1").unwrap().1.id;
		
		// conquers the systems
		let ships = u.ships_in_star_systems.linked(capital2);
		u.getf_mut(e2).unwrap().submitted_actions = Some(ships.into_iter().map(|ship_id| crate::universe::action::Action::MoveShip{ship: ship_id, to: ss1}).collect());
		u.make_turn().unwrap();
		
		u.getf_mut(e2).unwrap().submitted_actions = Some(vec![crate::universe::action::Action::CaptureStarSystem{empire: e2, star_system: ss1}]);
		u.make_turn().unwrap();
		
		// retreats to capital
		let ships = u.ships_in_star_systems.linked(ss1);
		u.getf_mut(e2).unwrap().submitted_actions = Some(ships.into_iter().map(|ship_id| crate::universe::action::Action::MoveShip{ship: ship_id, to: capital2}).collect());
		
		// gather strength
		for _ in 0..10 {
			u.make_turn().unwrap();
		}
		
		// now empire2 should have a sizeable advantage
		assert_eq!(14, u.ships_in_star_systems.linked(capital1).len());
		assert_eq!(16, u.ships_in_star_systems.linked(capital2).len());
		
		// empire2 launches a counteroffensive
		let ships = u.ships_in_star_systems.linked(capital2);
		u.getf_mut(e2).unwrap().submitted_actions = Some(ships.into_iter().map(|ship_id| crate::universe::action::Action::MoveShip{ship: ship_id, to: capital1}).collect());
		
		u.make_turn().unwrap();
		let ships_owners: Vec<_> = u.ships_in_star_systems.linked(capital1).into_iter().map(|s| u.ships_in_empires.parent(s).unwrap()).collect();
		
		// wins with tight advantage
 		assert_eq!(ships_owners.as_slice(), &[e2]);
		
		// and captures Capital 1
		u.getf_mut(e2).unwrap().submitted_actions = Some(vec![crate::universe::action::Action::CaptureStarSystem{empire: e2, star_system: capital1}]);
		u.make_turn().unwrap();
	}
	
}

