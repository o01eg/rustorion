// frontend for manipulation of UniverseView data
// Intended to be type-safe, assuming you only use public methods

use crate::universeview;
use crate::storage::EntityStored;

#[derive(Copy, Clone)]
pub struct StarSystem<'a> {
	pub data: &'a universeview::StarSystem,
	pub universe: &'a universeview::Universe,
}

impl<'a> StarSystem<'a> {

	pub fn empire(&self) -> Option<Empire> {
		self.universe.star_systems_in_empires.parent(self.data.id).map(|e_id| Empire { data: self.universe.getf(e_id).unwrap(), universe: self.universe })
	}
	
	pub fn ships(&self) -> Vec<Ship> {
		self.universe.ships_in_star_systems.linked(self.data.id).into_iter().map(|s_id| Ship { data: self.universe.getf(s_id).unwrap(), universe: self.universe }).collect()
	}
	
	pub fn starlanes(&self) -> Vec<StarSystem> {
		self.universe.starlanes.links(self.data.id).into_iter().map(|ss_id| StarSystem { universe: self.universe, data: self.universe.getf(ss_id).unwrap()
	}).collect()
	}
}

impl<'a> Empire<'a> {
	pub fn star_systems(&self) -> Vec<StarSystem> {
		self.universe.star_systems_in_empires.linked(self.data.id).iter().map(|id| StarSystem { universe: self.universe, data: self.universe.getf(*id).unwrap()}).collect()
	}
	
	pub fn ships(&self) -> Vec<Ship> {
		self.universe.ships_in_empires.linked(self.data.id).into_iter().map(|s_id| Ship { data: self.universe.getf(s_id).unwrap(), universe: self.universe }).collect()
	}
}


#[derive(Copy, Clone)]
pub struct Empire<'a> {
	pub data: &'a universeview::Empire,
	pub universe: &'a universeview::Universe,
}

#[derive(Copy, Clone)]
pub struct Ship<'a> {
	pub data: &'a universeview::Ship,
	pub universe: &'a universeview::Universe,
}

#[derive(Copy, Clone)]
pub struct Universe<'a> {
	pub data: &'a universeview::Universe,
}

impl<'a> Universe<'a> {

	pub fn empires(&self) -> Vec<Empire> {
		self.data.empires.iter().map(|x| Empire { universe: self.data, data: x.1 }).collect()
	}
	
	pub fn star_systems(&self) -> Vec<StarSystem> {
		self.data.star_systems.iter().map(|x| StarSystem { universe: self.data, data: x.1 }).collect()
	}
}
