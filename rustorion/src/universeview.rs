pub mod interface;

// A non-perfect view into an Universe. Contains only things visible to a certain observer.
// Structure is based on Universe
// Intended to be passed to players, only containing data they are supposed to know

use crate::storage::IDentified;
use crate::universe as u;
use crate::storage::ID;
use crate::storage as s;
use std::collections::HashMap;
use std::collections::HashSet;
use crate::storage::EntityStored;
use serde::{Deserialize, Serialize};
use crate::storage::links;

#[derive(PartialEq, Eq, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Planet {
	pub id: ID<Planet>,
}

impl s::IDentified for Planet {
	fn id(&self) -> ID<Self> {
		return self.id;
	}
	
	fn id_mut(&mut self) -> &mut ID<Self> {
		return &mut self.id;
	}
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct Ship {
	pub id: ID<Ship>,
	pub name: String,
}

impl IDentified for Ship {
	fn id(&self) -> ID<Self> {
		return self.id;
	}
	fn id_mut(&mut self) -> &mut ID<Self> {
		return &mut self.id;
	}
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct StarSystem {
	pub id: ID<StarSystem>,
	pub name: String,
	pub location: u::UniverseLocation,
	pub population: i64,
	pub can_capture: bool,
	pub industry: u64,
}

impl s::IDentified for StarSystem {
	fn id(&self) -> ID<Self> {
		return self.id;
	}
	
	fn id_mut(&mut self) -> &mut ID<Self> {
		return &mut self.id;
	}
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct Empire {
	pub id: ID<Empire>,
	pub name: String,
	pub color: u::Color,
	pub capital: Option<ID<StarSystem>>,
	pub stockpile_industry: u64,
}

impl s::IDentified for Empire {
	fn id(&self) -> ID<Self> {
		return self.id;
	}
	
	fn id_mut(&mut self) -> &mut ID<Self> {
		return &mut self.id;
	}
}

#[derive(Default, Serialize, Deserialize)]
pub struct Universe {
	pub planets: HashMap<s::StorageID, Planet>,
	pub star_systems: HashMap<s::StorageID, StarSystem>,
	current_id: s::StorageID,
	pub empires: HashMap<s::StorageID, Empire>,
	pub planets_in_star_systems: links::HasMany<StarSystem, Planet>,
	pub starlanes: links::Interlink<StarSystem>,
	pub star_systems_in_empires: links::HasMany<Empire, StarSystem>,
	pub ships: HashMap<s::StorageID, Ship>,
	pub ships_in_star_systems: links::HasMany<StarSystem, Ship>,
	pub ships_in_empires: links::HasMany<Empire, Ship>,
	pub capitals_in_empires: links::HasOne<Empire, StarSystem>,
	pub controlled_empire: Option<s::ID<Empire>>,
	pub turn_number: u64,
}

impl s::EntityStored<Planet> for Universe {
	fn storage(&self) -> &HashMap<s::StorageID, Planet> {
		&self.planets
	}
	fn storage_mut(&mut self) -> &mut HashMap<s::StorageID, Planet> {
		&mut self.planets
	}
}

impl s::EntityStored<Ship> for Universe {
	fn storage(&self) -> &HashMap<s::StorageID, Ship> {
		&self.ships
	}
	fn storage_mut(&mut self) -> &mut HashMap<s::StorageID, Ship> {
		&mut self.ships
	}
}

impl s::EntityStored<StarSystem> for Universe {
	fn storage(&self) -> &HashMap<s::StorageID, StarSystem> {
		&self.star_systems
	}
	fn storage_mut(&mut self) -> &mut HashMap<s::StorageID, StarSystem> {
		&mut self.star_systems
	}
}

impl s::EntityStored<Empire> for Universe {
	fn storage(&self) -> &HashMap<s::StorageID, Empire> {
		&self.empires
	}
	fn storage_mut(&mut self) -> &mut HashMap<s::StorageID, Empire> {
		&mut self.empires
	}
}

impl s::IssuesIDs for Universe {
	fn next_id(&mut self) -> s::StorageID {
		self.current_id += 1;
		self.current_id
	}
}

impl Universe {
	// Information that only a certain Empire would know
	pub fn empire_view(universe: &u::Universe, empire_id: ID<u::Empire>) -> Universe {
		let mut u_v = Universe::default();
		
		for (_, e) in &universe.empires {
			u_v.import_empire(e);
		}
		
 		let mut visible_system_set = HashSet::<ID<u::StarSystem>>::new();
		for ss_id in universe.star_systems_in_empires.linked(empire_id) {
 			visible_system_set.insert(ss_id);
 			let v_ss_id = ss_id.cast();
			let new_empire_id = empire_id.cast();
			u_v.star_systems_in_empires.add_link(new_empire_id, v_ss_id);
			for p_id in universe.planets_in_star_systems.linked(ss_id) {
				let p = universe.getf(p_id).unwrap();
				let v_p_id = u_v.import_planet(p);
				u_v.planets_in_star_systems.add_link(v_ss_id, v_p_id);
			}
			
			for ss2_id in universe.starlanes.links(ss_id) {
				visible_system_set.insert(ss2_id);
				let (v_id, v_child_id) = (ss_id.cast(), ss2_id.cast());
				u_v.starlanes.add_link(v_id, v_child_id);
			}
		}
		
		for ss_id in &visible_system_set {
			let ss = universe.getf(*ss_id).unwrap();
			// TODO: make more far-reaching import, not only for data
			let imported_ss_id = u_v.import_star_system(ss);
			let mut dominance = true;
			let mut agency = false;
			if let Some(owner_empire_id) = universe.star_systems_in_empires.parent(*ss_id) {
				u_v.star_systems_in_empires.add_link(owner_empire_id.cast(), imported_ss_id);
			}
			if let Some(capital_owner_id) = universe.capitals_in_empires.parent(*ss_id) {
				u_v.capitals_in_empires.add_link(capital_owner_id.cast(), ss_id.cast());
			}
			for ship_id in &universe.ships_in_star_systems.linked(*ss_id) {
				u_v.import_ship(universe.getf(*ship_id).unwrap());
				u_v.ships_in_star_systems.add_link(ss_id.cast(), ship_id.cast());
				if let Some(ship_empire_id) = universe.ships_in_empires.parent(*ship_id) {
					u_v.ships_in_empires.add_link(ship_empire_id.cast(), ship_id.cast());
					if ship_empire_id == empire_id {
						agency = true;
					} else {
						dominance = false;
					}
				}
			}
			if agency && dominance && u_v.star_systems_in_empires.parent(imported_ss_id) != Some(empire_id.cast()) {
				u_v.getf_mut(imported_ss_id).unwrap().can_capture = true;
			}
			
		}
		
		u_v.controlled_empire = Some(empire_id.cast());
		u_v.turn_number = universe.turn_number;
		u_v
	}

	// not efficient, use copy_view() (TBD) instead
	// reference view implementation, intended as base for specific views
	pub fn perfect_view(universe: &u::Universe) -> Universe {
		let mut u = Universe::default();
		
		let mut visible_system_set = HashSet::<ID<StarSystem>>::new();
		for (_id, star_system) in &universe.star_systems {
			let v_id = u.import_star_system(star_system);
			visible_system_set.insert(v_id);
		}
		
		let mut visible_planet_set = HashSet::<ID<Planet>>::new();
		for (_id, planet) in &universe.planets {
			let v_id = u.import_planet(planet);
			visible_planet_set.insert(v_id);
		}
		
		for (_id, empire) in &universe.empires {
			u.import_empire(empire);
		}
		
		for (id, child_map) in &universe.starlanes.map {
			for child_id in child_map {
				u.starlanes.add_link(id.cast(), child_id.cast());
			}
		}
		
		for (id, child_map) in &universe.planets_in_star_systems.children_mmap {
			for child_id in child_map {
				u.planets_in_star_systems.add_link(id.cast(), child_id.cast());
			}
		}
		
		for (id, child_map) in &universe.star_systems_in_empires.children_mmap {
			for child_id in child_map {
				u.star_systems_in_empires.add_link(id.cast(), child_id.cast());
			}
		}
		
		for (id, child_map) in &universe.ships_in_empires.children_mmap {
			for child_id in child_map {
				u.ships_in_empires.add_link(id.cast(), child_id.cast());
			}
		}
		
		for (id, child_map) in &universe.ships_in_star_systems.children_mmap {
			for child_id in child_map {
				u.ships_in_star_systems.add_link(id.cast(), child_id.cast());
			}
		}
		
		u.turn_number = u.turn_number;
		
		u
	}
	
	pub fn import_star_system(&mut self, star_system: &u::StarSystem) -> ID<StarSystem> {
		let id = star_system.id().cast();
		let ss = StarSystem {
			id,
			name: star_system.name.clone(),
			location: star_system.location,
			population: star_system.population,
			can_capture: false,
			industry: star_system.industry,
		};
		
		self.set(ss).unwrap();
		id
	}
	
	pub fn import_planet(&mut self, planet: &u::Planet) -> ID<Planet> {
		let id = planet.id().cast();
		let p = Planet {
			id,
		};
		
		self.set(p).unwrap();
		id
	}
	
	pub fn import_ship(&mut self, ship: &u::Ship) -> ID<Ship> {
		let id = ship.id().cast();
		let s = Ship {
			id,
			name: ship.name.clone(),
		};
		
		self.set(s).unwrap();
		id
	}
	
	pub fn import_empire(&mut self, empire: &u::Empire) -> ID<Empire>{
		let id = empire.id().cast();
		let e = Empire {
			id,
			name: empire.name.clone(),
			color: empire.color,
			capital: None,
			stockpile_industry: empire.stockpile_industry,
		};
		
		self.set(e).unwrap();
		id
	}
}
