use iced::Application;
use rstar::RTree;
use rustorion::storage as s;
use rustorion::universeview as uv;
use rustorion::universeview::interface as ui;
use anyhow::Result;
use rustorion::storage::EntityStored;
use structopt::StructOpt;
use std::sync::Arc;
use tokio::sync::Mutex;
use iced::widget::canvas::Frame;
use iced::widget::canvas::path::Path;


#[derive(Clone, Default)]
struct Camera {
	zoom: f32,
	offset: [f32; 2],
}

impl Camera {
	pub fn view_point(&self, point: [f32; 2]) -> iced::Point {
		[(point[0] * self.zoom + self.offset[0]), (point[1] * self.zoom + self.offset[1])].into()
	}
	
	pub fn unview_point(&self, point: iced::Point) -> [f32; 2] {
		[(point.x - self.offset[0]) / self.zoom, (point.y - self.offset[1]) / self.zoom]
	}
	
	pub fn view_rectangle(&self, rect: iced_native::Rectangle<f32>) -> iced_native::Rectangle<f32> {
		iced_native::Rectangle {
			x: rect.x * self.zoom + self.offset[0],
			y: rect.y * self.zoom + self.offset[1],
			width: rect.width * self.zoom,
			height: rect.height * self.zoom,
		}
	}
}

// Drawable is a scene object, positioned in abstract coordinates. It can provide a Primitive given offset and zoom level.
struct Drawable {
	rect: iced_native::Rectangle<f32>,
	draw_data: DrawData,
	#[allow(dead_code)] // TODO: use this
	z_level: i64,
}

enum DrawData {
	StarSystem(s::ID<uv::StarSystem>),
	Starlane([f32; 2], [f32; 2]),
}

#[derive(Default)]
struct SharedData {
	camera: Camera,
	universe_view: Option<uv::Universe>,
	drawable_tree: RTree<Drawable>,
}

impl SharedData {
	fn fill_scene(&mut self) {
		let mut drawables = vec![];
		
		let universe_view = match &self.universe_view {
			None => return,
			Some(uv) => uv,
		};
	
		let ui = rustorion::universeview::interface::Universe { data: universe_view };
		
		for star_system in &ui.star_systems() {
			drawables.push(Drawable{
				draw_data: DrawData::StarSystem(star_system.data.id),
				z_level: 0,
				rect: iced_native::Rectangle { x: star_system.data.location[0], y: star_system.data.location[1], width: 100., height: 100.},
			});
			for linked_system in star_system.starlanes() {
				// TODO: we are effectively drawing this two times
				drawables.push(Drawable{
					rect: from_corners(star_system.data.location.into(), linked_system.data.location.into()),
					z_level: 1,
					draw_data: DrawData::Starlane(star_system.data.location.into(), linked_system.data.location.into()),
				});
			}
		}
		self.drawable_tree = RTree::bulk_load(drawables);
	}
}


impl Drawable {
	fn primitive(&self, data: &SharedData) -> iced_wgpu::Primitive {
		let camera = &data.camera;
		let view = data.universe_view.as_ref().unwrap();
		match self.draw_data {
			DrawData::StarSystem(id) => {
				let star_system = ui::StarSystem { universe: view, data: view.get(id).unwrap() };
				let mapped_rect = camera.view_rectangle(self.rect);
				let mut frame = Frame::new(mapped_rect.width, mapped_rect.height);
				frame.translate(iced::Vector::new(mapped_rect.x, mapped_rect.y));
				let star_image = iced_wgpu::Primitive::Image {
					bounds: mapped_rect,
					handle: iced_native::widget::image::Handle::from_path("resources/star.png"),
				};
				let color = match star_system.empire() {
					None => iced::Color::WHITE,
					Some(e) => e.data.color.into(),
				};
				frame.stroke(&Path::circle(frame.center(), frame.height() / 2.0 ), iced::canvas::Stroke {
					width: 1.0,
					color: color,
					..iced::canvas::Stroke::default()
				});
				
				let mut text: iced_wgpu::widget::canvas::Text = star_system.data.name.clone().into();
				text.color = color;
				frame.fill_text(text);
				iced_wgpu::Primitive::Group {
					primitives: vec![star_image, frame.into_primitive()],
				}
			},
			DrawData::Starlane(point1, point2) => {
				let mapped_rect = camera.view_rectangle(self.rect);
				let mut frame = Frame::new(mapped_rect.width, mapped_rect.height);
				frame.stroke(&Path::line(camera.view_point(point1), camera.view_point(point2)), iced::canvas::Stroke {
					width: 2.0,
					color: iced::Color::WHITE,
					..iced::canvas::Stroke::default()
				});
				frame.into_primitive()
			}
		}
	}
}

impl rstar::RTreeObject for Drawable {
	type Envelope = rstar::AABB<[f32; 2]>;
	fn envelope(&self) -> Self::Envelope
	{
		rstar::AABB::from_corners([self.rect.x, self.rect.y], [self.rect.x + self.rect.width, self.rect.y + self.rect.height])
	}
}

#[derive(Default)]
struct Scene {
	shared_data: Arc<std::sync::Mutex<SharedData>>,
}


impl iced_native::Widget<AppMessage, iced_wgpu::Renderer> for Scene {
	fn width(&self) -> iced_native::Length {
		iced_native::Length::Fill
	}
	
	fn height(&self) -> iced_native::Length {
		iced_native::Length::Fill
	}
	
	fn layout(&self, _renderer: &iced_wgpu::Renderer, limits: &iced_native::layout::Limits) -> iced_native::layout::Node {
		iced_native::layout::Node::new(limits.max())
	}
	
	fn draw(&self, _renderer: &mut iced_wgpu::Renderer, _defaults: &iced_wgpu::Defaults, layout: iced_native::layout::Layout, _cursor_position: iced_native::Point ) -> (iced_wgpu::Primitive, iced_native::MouseCursor) {
		let mut primitives = vec![iced_wgpu::Primitive::Quad{
			bounds: layout.bounds(),
			background: iced_native::Background::Color(iced_native::Color::BLACK),
			border_radius: 0,
			border_width: 0,
			border_color: iced_native::Color::BLACK,
			}];
		let shared_data = self.shared_data.lock().unwrap();
		for drawable in shared_data.drawable_tree.iter() {
			let prim = drawable.primitive(&shared_data);
			primitives.push(prim);
			
		}
		let group = iced_wgpu::Primitive::Group { primitives };
		(group, iced_native::MouseCursor::OutOfBounds)
	}
	
	fn hash_layout(&self, state: &mut iced_native::Hasher) {
		use std::hash::Hash;
		().hash(state);
	}
	
	 fn on_event(
		&mut self,
		event: iced_native::Event,
		_layout: iced_native::Layout,
		cursor_position: iced_native::Point,
		messages: &mut Vec<AppMessage>,
		_renderer: &iced_wgpu::Renderer,
		_clipboard: Option<&dyn iced_native::Clipboard>
		) {
		use iced_native::input::keyboard::KeyCode;
		use iced_native::input::mouse::Event as MouseEvent;
		match event {
			iced_native::Event::Keyboard(
				iced_native::input::keyboard::Event::Input {
					state: iced_native::input::ButtonState::Released,
					key_code,
					..
				}) => match key_code {
						KeyCode::Subtract => messages.push(AppMessage::Zoom(1.10, cursor_position)),
						KeyCode::Equals => messages.push(AppMessage::Zoom(0.90, cursor_position)),
						KeyCode::Up => messages.push(AppMessage::MoveCamera([0., 10.])),
						KeyCode::Down => messages.push(AppMessage::MoveCamera([0., -10.])),
						KeyCode::Left => messages.push(AppMessage::MoveCamera([10.0, 0.])),
						KeyCode::Right => messages.push(AppMessage::MoveCamera([-10.0, 0.])),
						_ => (),
				},
			iced_native::Event::Mouse(event) => match event {
				MouseEvent::WheelScrolled { delta } => match delta {
					iced_native::input::mouse::ScrollDelta::Lines { y, .. } => messages.push(AppMessage::Zoom(1.10_f32.powf(y), cursor_position)),
					iced_native::input::mouse::ScrollDelta::Pixels { y, .. } => messages.push(AppMessage::Zoom(1.10_f32.powf(y/10.0), cursor_position)),
				},
				MouseEvent::Input { state: iced_native::input::ButtonState::Released, button: iced_native::input::mouse::Button::Left } => messages.push(AppMessage::StopDrag),
				MouseEvent::Input { state: iced_native::input::ButtonState::Pressed, button: iced_native::input::mouse::Button::Left } => messages.push(AppMessage::StartDrag),
				MouseEvent::CursorMoved { x, y } => messages.push(AppMessage::CursorMoved(x, y)),
				_ => (),
			},
			_ => (),
		}
	}
}

impl<'a> Into<iced_native::Element<'a, AppMessage, iced_wgpu::Renderer>> for Scene {
	fn into(self) -> iced_native::Element<'a, AppMessage, iced_wgpu::Renderer> {
		iced_native::Element::new(self)
	}
}

#[derive(StructOpt, Clone)]
struct Options {
	/// server host:port
	host_port: String,

	/// certificate
	#[structopt()]
	cert: String,

	/// certificate key
	#[structopt()]
	key: String,
}

enum AppMessage {
	SetView(uv::Universe),
	SetClient(rustorion::net::client::Client),
	Error(anyhow::Error),
	Zoom(f32, iced_native::Point),
	MoveCamera([f32; 2]),
	StartDrag,
	StopDrag,
	CursorMoved(f32, f32),
}

impl std::fmt::Debug for AppMessage {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			AppMessage::SetView(_) => f.debug_struct("SetView()").finish(),
			AppMessage::SetClient(_) => f.debug_struct("SetClient()").finish(),
			AppMessage::Error(e) => {
				let err_s = format!("{}", e);
				f.debug_struct("Error()")
					.field("error", &err_s).finish()
			},
			AppMessage::Zoom(adj, cp) => f.debug_struct("Zoom()")
				.field("zoom level", adj)
				.field("cursor", cp).finish(),
			AppMessage::MoveCamera(adj) => f.debug_struct("MoveCamera()").field("move amount", adj).finish(),
			AppMessage::StartDrag => f.debug_struct("StartDrag()").finish(),
			AppMessage::StopDrag => f.debug_struct("StopDrag()").finish(),
			AppMessage::CursorMoved(x, y) => f.debug_struct("CursorMoved()")
				.field("x", x)
				.field("y", y).finish(),
			
		}
	}
}

fn from_corners(p1: iced::Point, p2: iced::Point) -> iced_native::Rectangle {
	
	iced_native::Rectangle {
		x: if p1.x > p2.x { p2.x } else { p1.x },
		y: if p1.y > p2.y { p2.y } else { p1.y },
		width: (p2.x - p1.x).abs(),
		height: (p2.y - p1.y).abs(),
	}
}

impl Rustorion {

	async fn establish_connection(options: Options) -> Result<AppMessage> {
		let auth_data = rustorion::net::client::AuthData::read_or_generate(&options.cert, &options.key)?;
		let client = rustorion::net::client::Client::connect(&options.host_port, &auth_data).await?;
		Ok(AppMessage::SetClient(client))
	}
	
	async fn obtain_view(client: Arc<Mutex<rustorion::net::client::Client>>) -> Result<AppMessage> {
		let mut client = client.lock().await;
		let view = client.get_view().await?;
		Ok(AppMessage::SetView(view))
	}
	
	// Return an Error message in case of error
	fn wrap_result(res: Result<AppMessage>) -> AppMessage {
		match res {
			Ok(msg) => msg,
			Err(err) => AppMessage::Error(err),
		}
	}
}

impl iced::Application for Rustorion {
	type Message = AppMessage;
	type Executor = iced::executor::Default;
	type Flags = Options;
	
	fn new(flags: Self::Flags) -> (Self, iced::Command<Self::Message>) {
		let app = Rustorion {
			client: None,
			shared_data: Arc::new(std::sync::Mutex::new(SharedData {
				camera: Camera {
					offset: [1000., 500.],
					zoom: 2000./20000.0,
				},
				drawable_tree: RTree::default(),
				universe_view: None,
			})),
			..Default::default()
		};
		(
			app,
			iced::Command::batch(vec![
				iced::Command::from(Self::establish_connection(flags.clone())).map(Self::wrap_result),
			]),
		)
	}
	
	fn title(&self) -> String {
		String::from("Game of Life - Iced")
	}
	
	fn update(&mut self, message: Self::Message) -> iced::Command<Self::Message> {
 		let shared_data = &mut self.shared_data.lock().unwrap();
		match message {
			AppMessage::SetView(view) => { 				
				shared_data.universe_view = Some(view);
				shared_data.fill_scene();
			},
			AppMessage::SetClient(client) => {
				let client = Arc::new(Mutex::new(client));
				self.client = Some(client.clone());
				return iced::Command::from(Rustorion::obtain_view(client)).map(Self::wrap_result)
			},
			AppMessage::Zoom(adj, cursor_position) => {
				let old_zoom = shared_data.camera.zoom;
				let pointed_at = shared_data.camera.unview_point(cursor_position);
				shared_data.camera.zoom *= adj;
				shared_data.camera.offset[0] += pointed_at[0] * (old_zoom - shared_data.camera.zoom);
				shared_data.camera.offset[1] += pointed_at[1] * (old_zoom - shared_data.camera.zoom);
			},
			AppMessage::MoveCamera(adj) => {
				shared_data.camera.offset[0] += adj[0];
				shared_data.camera.offset[1] += adj[1];
			},
			AppMessage::Error(error) => { eprintln!("error: {:?}", error); },
			AppMessage::StartDrag => { self.dragging = true; },
			AppMessage::StopDrag => { self.dragging = false; self.prev_x = None; self.prev_y = None; },
			AppMessage::CursorMoved(x, y) => {
				if self.dragging {
					let prev_x = self.prev_x.get_or_insert(x);
					let prev_y = self.prev_y.get_or_insert(y);
					let dragged_x = x - *prev_x;
					let dragged_y = y - *prev_y;
					shared_data.camera.offset[0] += dragged_x;
					shared_data.camera.offset[1] += dragged_y;
					self.prev_x = Some(x);
					self.prev_y = Some(y);
				}
			},
		}
		iced::Command::none()
	}


	fn view(&mut self) -> iced::Element<Self::Message> {
		Scene { shared_data: self.shared_data.clone() }.into()
	}
}

#[derive(Default)]
struct Rustorion {
// 	cache: canvas::layer::Cache<State>,
	client: Option<Arc<Mutex<rustorion::net::client::Client>>>,
	shared_data: Arc<std::sync::Mutex<SharedData>>,
	dragging: bool,
	prev_x: Option<f32>,
	prev_y: Option<f32>,
}

pub fn main() -> Result<()> {
	let options = Options::from_args();
	Rustorion::run(iced::Settings::with_flags(options));
	Ok(())
}
