#!/bin/sh

test_description="Test universe manipulation"

. ./sharness.sh

BINDIR="../../target/debug"

test_expect_success "Generate a name" '
	"$BINDIR/rustorion-namegen" 100 -s suffix | grep -q "suffix$"
'

test_expect_success "Create universe" '
	"$BINDIR/rustorion-universegen" -t -e 3 -s 30 > small-universe.cbor
'

test_expect_success "Create empire 1 view of universe" '
	"$BINDIR/rustorion-viewuniverse" -e Empire1 < small-universe.cbor > 1-view.cbor
'

test_expect_success "Create a perfect view of universe" '
	"$BINDIR/rustorion-viewuniverse" < small-universe.cbor > perfect-view.cbor
'

test_expect_success "Create an action capturing star system 2 for the glory of empire 1" '
	"$BINDIR/rustorion-actiongen" 2-battle.actions capture StarSystem2 Empire1 < perfect-view.cbor
'

test_expect_success "Create an action capturing star system 1 for the glory of empire 1" '
	"$BINDIR/rustorion-actiongen" 1-battle.actions capture StarSystem1 Empire1 < perfect-view.cbor
'

# test_expect_success "Fail to capture star system 2 due to running orders from empire 2" '
# 	test_expect_code 1 "$BINDIR/rustorion-act" -V Empire2 2-battle.actions < small-universe.cbor > fail-small1.cbor
# '
# 
# test_expect_success "Capture system 2" '
# 	"$BINDIR/rustorion-act" -V Empire1 2-battle.actions < small-universe.cbor > small1.cbor
# '
# 
# test_expect_success "Don't capture system 2 again" '
# 	test_expect_code 1 "$BINDIR/rustorion-act" -V Empire1 2-battle.actions < small1.cbor > bad.cbor
# '
# 
# test_expect_success "Create a new Empire 1 view of universe" '
# 	"$BINDIR/rustorion-viewuniverse" -e Empire1 < small1.cbor > new-empire1-view.cbor
# '
# 
# test_expect_success "Empire 1 should see star system 2 now" '
# 	"$BINDIR/rustorion-actiongen" repeated-2-battle.actions Empire1 capture StarSystem2 < new-empire1-view.cbor 
# '


test_done
