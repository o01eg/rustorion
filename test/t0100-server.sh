#!/bin/sh

test_description="Test universe manipulation"

. ./sharness.sh

BINDIR="../../target/debug"

"$BINDIR/rustorion-universegen" -t -e 2 -s 30 > small-universe.cbor

test_expect_success "Launch the server" '
	"$BINDIR/rustorion-server" localhost:4433 localhost:4434 --universe small-universe.cbor s.der sk.der & echo $! > server.pid
	sleep 1
'

test_expect_success "Get a view from the server" '
	"$BINDIR/rustorion-client" localhost:4433 c k get-view > view.cbor
'

test_expect_success "Submit empty action list" '
	"$BINDIR/rustorion-actiongen" actions.cbor nothing < view.cbor  &&
	"$BINDIR/rustorion-client" localhost:4433 c k set-actions > view.cbor < actions.cbor
'


test_expect_success "Fail to get a view from third client because out of slots" '
	"$BINDIR/rustorion-client" localhost:4433 c2 k2 get-view > view.cbor &&
	test_expect_code 1 "$BINDIR/rustorion-client" localhost:4433 c3 k3 get-view > view.cbor
'

kill `cat server.pid`

test_done
