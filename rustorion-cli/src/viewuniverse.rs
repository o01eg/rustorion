use rustorion::universeview;
// use rustorion::universe::interface;
use std::io;
use anyhow::{Result, anyhow};
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(about = "Generate a view of universe from STDIN")]
struct Options {	
	#[structopt(short = "e")]
	/// Name of empire for non-perfect view
	empire_name: Option<String>,
}


fn main() -> Result<()> {
	let options = Options::from_args();
	
	let u = rmp_serde::decode::from_read(io::stdin()).expect("Failed to parse universe");
	let ui = rustorion::universe::interface::Universe { data: &u };
	let view = match options.empire_name {
		Some(name) => {
			let empires = ui.empires();
			let target = empires.iter().find(|e| e.data.name == name).ok_or(anyhow!("Failed to locate empire"))?;
			universeview::Universe::empire_view(&u, target.data.id)
		},
		None => {
			universeview::Universe::perfect_view(&u)
		},
	};
	rmp_serde::encode::write(&mut io::stdout(), &view)?;
	Ok(())
}
