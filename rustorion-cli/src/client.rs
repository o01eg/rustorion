use structopt::StructOpt;
use tokio::runtime;
use anyhow::{Result, anyhow};
use tokio::io::AsyncWriteExt;
use rustorion::net::client;
#[derive(StructOpt)]
struct Options {
	host: String,

	/// certificate
	#[structopt()]
	cert: String,

	/// certificate key
	#[structopt()]
	key: String,
	
	#[structopt(subcommand)]
	command: Command,
}

#[derive(StructOpt)]
enum Command {
	GetView,
	SetActions,
	WaitForTurn {
		#[structopt()]
		back_addr: String,
	},
	GetEmpireId,
}

async fn wait_for_turn(handle: runtime::Handle, options: &Options, host: &String, auth_data: &client::AuthData) -> Result<()> {
	let client = client::Client::connect(&options.host, auth_data).await?;
	let (turn_sender, mut receiver) = tokio::sync::watch::channel(0);
	receiver.recv().await.unwrap(); // Discard initial value
	let client_handle = handle.spawn(
		client::Client::back_connect(
			client.client_config,
			host.clone(),
			ClientCallbacks {turn_sender}));
	let next_turn = receiver.recv().await.ok_or(anyhow!("turn sender closed"))?;
	println!("{}", next_turn);
	client_handle.await??;
	Ok(())
}

async fn get_view(options: Options, auth_data: &client::AuthData) -> Result<()> {
	let mut client = client::Client::connect(&options.host, auth_data).await?;
	let view = client.get_view().await?;
	let view_bytes = rmp_serde::encode::to_vec(&view)?;
	tokio::io::stdout().write_all(view_bytes.as_slice()).await?;
	Ok(())
}

async fn get_empire_id(options: Options, auth_data: &client::AuthData) -> Result<()> {
	let mut client = client::Client::connect(&options.host, auth_data).await?;
	let id = client.get_empire_id().await?;
	println!("{}", id.0);
	Ok(())
}


async fn set_actions(options: Options, auth_data: &client::AuthData) -> Result<()> {
	let mut client = client::Client::connect(&options.host, auth_data).await?;
	let actions = rmp_serde::decode::from_read(std::io::stdin())?;
	client.set_actions(&actions).await?;
	Ok(())
}

struct ClientCallbacks {
	turn_sender: tokio::sync::watch::Sender<u16>,
}

impl client::ClientCallbacks for ClientCallbacks {
	fn turn_changed(&mut self, new_turn: u16) -> Result<()> {
		self.turn_sender.broadcast(new_turn)?;
		Ok(())
	}
}

fn main() -> Result<()> {
	let options = Options::from_args();
	let mut runtime = runtime::Builder::new()
		.basic_scheduler()
		.enable_io()
		.build()?;
	
	let auth_data = client::AuthData::read_or_generate(&options.cert, &options.key)?;
	
	match &options.command {
		Command::GetView => runtime.block_on(get_view(options, &auth_data)),
		Command::SetActions => runtime.block_on(set_actions(options, &auth_data)),
		Command::WaitForTurn { back_addr } => runtime.block_on(wait_for_turn(runtime.handle().clone(), &options, &back_addr, &auth_data)),
		Command::GetEmpireId => runtime.block_on(get_empire_id(options, &auth_data))
	}
	
}
