use rustorion::universe::*;
use std::io;
use structopt::StructOpt;
use std::fs;
use anyhow::{Result, Context, bail};

#[derive(StructOpt)]
#[structopt(about = "Generate a rustorion action for view in STDIN and write/append it to given file")]
struct Options {
	file: String,

	#[structopt(subcommand)]
	command: Command,
}

#[derive(StructOpt)]
enum Command {
	Capture {
		#[structopt()]
		star_system_name: String,
		empire_name: String,
	},
	Move {
		#[structopt()]
		ship_name: String,
		#[structopt()]
		star_system_name: String,
	},
	// does not actually insert any action
	Nothing,
}

fn main() -> Result<()> {
	let options = Options::from_args();
	let u = rmp_serde::decode::from_read(io::stdin())?;
	let ui = rustorion::universeview::interface::Universe { data: &u };
	
	let mut actions: Vec<rustorion::universe::action::Action> = match fs::read(&options.file) {
		Ok(actions_bytes) => rmp_serde::decode::from_read(actions_bytes.as_slice())?,
		Err(ref e) if e.kind() == io::ErrorKind::NotFound => vec![],
		Err(e) => bail!("failed to read actions: {}", e),
	};
	match &options.command {
		Command::Capture{star_system_name, empire_name} => {
			let empires = ui.empires();
			let empire = empires.iter().find(|e| e.data.name == *empire_name).context("Failed to locate empire")?;
			let star_system = *ui.star_systems().iter().find(|ss| &ss.data.name == star_system_name).context("Failed to locate star system")?;
			let a = action::Action::CaptureStarSystem{star_system: star_system.data.id.cast(), empire: empire.data.id.cast()};
			actions.push(a);
		}
		Command::Move{ship_name, star_system_name} => {
			let ship = &u.ships.iter().find(|s| &s.1.name == ship_name).context("Failed to locate ship")?.1;
			let star_system = *ui.star_systems().iter().find(|ss| &ss.data.name == star_system_name).context("Failed to locate star system")?;
			let a = action::Action::MoveShip{ship: ship.id.cast(), to: star_system.data.id.cast()};
			actions.push(a);
		}
		Command::Nothing => {}
	}
	let mut outfile = std::fs::File::create(options.file)?;
	rmp_serde::encode::write(&mut outfile, &actions)?;
	Ok(())
}
