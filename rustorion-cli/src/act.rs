use rustorion::universe::action;
use std::io;
use std::fs::File;
use anyhow::{bail, Result};
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(about = "Apply actions to universe")]
struct Options {	
	#[structopt()]
	empire_name: String,
	
	#[structopt()]
	actions_file: String,
	
	#[structopt(short = "V")]
	validate: bool,
}


fn main() -> Result<()> {
	let options = Options::from_args();
	
	let mut u = rmp_serde::decode::from_read(io::stdin()).expect("Failed to parse universe");
	let actor = {
		let ui = rustorion::universe::interface::Universe { data: &u };
		let empire = *ui.empires().iter().find(|e| e.data.name == options.empire_name).expect("Failed to locate empire");
		rustorion::universe::action::Actor { empire_id: empire.data.id }
	};
	let file = File::open(&options.actions_file).expect("Failed to open file");
	let actions: Vec<action::Action> = rmp_serde::decode::from_read(file).expect(&format!("Failed to parse action file {}", options.actions_file));
	if options.validate {
		let failed_actions = action::Action::validate_batch(&actions.as_slice(), &u, &actor);
		if !failed_actions.is_empty() {
			let error_messages: Vec<String> = failed_actions.iter().map(|f| f.error.clone()).collect();
			bail!(error_messages.as_slice().join("\n"))
		}
	}
	for action in actions {
		action.apply(&mut u);
	}
	rmp_serde::encode::write(&mut io::stdout(), &u)?;
	Ok(())
}
