use rustorion::universegen;
use rand;
use std::io;
use anyhow::{Result};
use structopt::StructOpt;


#[derive(StructOpt)]
struct Options {

	/// number of star systems
	#[structopt(short = "s")]
	star_systems: Option<u64>,
    
	/// number of empires
	#[structopt(short = "e")]
	empires: Option<u64>,
	
	/// add some fixed stuff for automatic testing
	#[structopt(short = "t")]
	add_testables: bool,
	
	/// rate of uninhabited systems
	#[structopt(short = "u")]
	uninhabited: Option<f64>,
}

fn main() ->  Result<()> {
	let options = Options::from_args();
	let mut params = universegen::UniverseParameters::default();
	if let Some(star_systems) = options.star_systems {
		params.star_systems_n = star_systems;
	}
	
	if let Some(empires) = options.empires {
		params.empires_n = empires;
	}
	
	if let Some(uninhabited) = options.uninhabited {
		params.uninhabited = uninhabited;
	}
	
	params.add_testables = options.add_testables;
	
	let u = universegen::generate_universe(&mut rand::thread_rng(), &params)?;
	
	rmp_serde::encode::write(&mut io::stdout(), &u)?;
	Ok(())
}
