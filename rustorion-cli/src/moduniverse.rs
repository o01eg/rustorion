use std::io;
use anyhow::{Result};
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(about = "Modify universe")]
struct Options {	
	#[structopt(subcommand)]
	command: Command,
}

#[derive(StructOpt)]
enum Command {
	/// advance the turn
	MakeTurn,
}


fn main() -> Result<()> {
	let options = Options::from_args();
	let mut u: rustorion::universe::Universe = rmp_serde::decode::from_read(io::stdin()).expect("Failed to parse universe");
	
	match options.command {
		Command::MakeTurn => u.make_turn()?,
	}
	
	rmp_serde::encode::write(&mut io::stdout(), &u)?;
	Ok(())
}
