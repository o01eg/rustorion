use rustorion::names;
use rand;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Options {
	// word length
	length: usize,
	// suffix
	#[structopt(short = "s", default_value = "")]
	suffix: String,
}

fn main() {
	let options = Options::from_args();
	
	let u = names::generate_name(&mut rand::thread_rng(), options.length, &options.suffix);
	println!("{}",u);
}
