h1. WIP CBORPC-based Rustorion server

* Ensures clients supply any kind of certificate (ECDSA, EDDSA hopefully when rustls implements it)
* implements rustorion-server-0::getView command that generates a view from server's universe
* generates own cert + key

