use std::fs;
use std::net::ToSocketAddrs;
use std::{
	sync::Arc, sync::Mutex,
};
use std::io;
use futures_util::future::TryFutureExt;
use structopt::StructOpt;
use tokio::runtime;
use tokio::net::{TcpListener,TcpStream};
use tokio::stream::StreamExt;
use futures_util::sink::SinkExt;
use anyhow::{anyhow, bail, Context, Result};

use rustorion::universe as u;
use rustorion::storage as s;
use rustorion::universeview as uv;
use rustorion::universe::interface as ui;
use rustorion::storage::EntityStored;
use std::collections::hash_map;
use rustls::Session;
use tokio_util::codec;

struct CustomVerifier;

impl rustls::ClientCertVerifier for CustomVerifier {

	fn client_auth_root_subjects(
		&self,
		_sni: Option<&webpki::DNSName>,
	) -> Option<rustls::DistinguishedNames> {
		return Some(vec![]);
	}
	
	fn verify_client_cert(
		&self,
		_presented_certs: &[rustls::Certificate],
		_sni: Option<&webpki::DNSName>
	) -> Result<rustls::ClientCertVerified, rustls::TLSError> {
		return Ok(rustls::ClientCertVerified::assertion());
	}
}

#[derive(StructOpt)]
struct Options {
	addr: String,
	
	subscribe_addr: String,
	
	#[structopt(long = "universe", short = "u")]
	universe: String,
	
	/// certificate
	#[structopt()]
	cert: String,
	
	/// certificate key
	#[structopt()]
	key: String,

}

type PlayerID = blake3::Hash;

// Game state
struct State {
	universe: rustorion::universe::Universe,
	// mapping between players and empires they directly control
	player_map: bimap::BiMap<blake3::Hash, s::ID<u::Empire>>,
	turn_channel: (
		tokio::sync::watch::Sender<u64>, 
		tokio::sync::watch::Receiver<u64>, 
	),
}

impl State {
	fn make_turn(&mut self) -> Result<()> {
		self.universe.make_turn()?;
		self.turn_channel.0.broadcast(self.universe.turn_number)?;
		Ok(())
	}
}

// Sliced and diced TLS secure channel
struct TlsConnection {
	reader: codec::FramedRead<tokio::io::ReadHalf<tokio_rustls::server::TlsStream<TcpStream>>, codec::LengthDelimitedCodec>,
	writer: codec::FramedWrite<tokio::io::WriteHalf< tokio_rustls::server::TlsStream<TcpStream>>, codec::LengthDelimitedCodec>,
	connection_state: ConnectionState,
}

// Individual connection state, including a reference to server state
struct ConnectionState {
	state_rc: Arc<Mutex<State>>,
	cert_hash: PlayerID,
	turn_receiver: tokio::sync::watch::Receiver<u64>,
}

impl ConnectionState {
	fn obtain_empire_id(&mut self) -> Option<s::ID<u::Empire>> {
		let mut state = self.state_rc.lock().unwrap();
		
		// PROJECT: make this a separate RPC call?
		match state.player_map.get_by_left(&self.cert_hash) {
			Some(e_id) => Some(*e_id),
			None => match get_free_empire(&state) {
				Some(e_id) => {
					// If this fails, we have internal inconsistency
					// PROJECT: get better context, whatever this means
					state.player_map.insert_no_overwrite(self.cert_hash.clone(), e_id).map_err(|_| anyhow!("Cert hash already mapped?!")).unwrap();
					Some(e_id)
				},
				None => None,
			},
		}
	}
}


// fn load_certs(certs_data: &[u8]) -> io::Result<Vec<Certificate>> {
// 	certs(&mut BufReader::new(certs_data))
// 		.map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid cert"))
// }
// 
// fn load_keys(keys_data: &[u8]) -> io::Result<Vec<PrivateKey>> {
// 	pkcs8_private_keys(&mut BufReader::new(keys_data))
// 		.map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid key"))
// }

fn get_free_empire(state: &State) -> Option<s::ID<u::Empire>> {
	for empire in &state.universe.empires {
		let id: s::ID::<u::Empire> = s::ID::<u::Empire>::from_id(*empire.0);
		if !state.player_map.contains_right(&id) {
			return Some(id);
		}
	}
	return None;
}



fn server_responder(connection_state: ConnectionState) -> cborpc::Responder<ConnectionState> {
	fn get_actions(connection_state: &mut ConnectionState, _message: &cborpc::Data) -> Result<cborpc::CallResponse> {
		let empire_id = connection_state.obtain_empire_id().ok_or(anyhow!("Empire ID not available"))?;
		let state = connection_state.state_rc.lock().unwrap();
		let actions = &state.universe.getf(empire_id)?.submitted_actions;
		Ok(cborpc::CallResponse{success:true, message: rmp_serde::encode::to_vec(&actions)?})
	}

	fn cancel_actions(connection_state: &mut ConnectionState, _message: &cborpc::Data) -> Result<cborpc::CallResponse> {
		let e_id = connection_state.obtain_empire_id().ok_or(anyhow!("Empire ID not available"))?;
		let mut state = connection_state.state_rc.lock().unwrap();
		let mut empire = state.universe.getf_mut(e_id)?;
		empire.submitted_actions = None;
		Ok(cborpc::CallResponse{success:true, message: vec![]})
	}
	
	fn set_actions(connection_state: &mut ConnectionState, message: &cborpc::Data) -> Result<cborpc::CallResponse> {
		let actions: Vec<u::action::Action> = rmp_serde::decode::from_read_ref(message.as_slice())?;
		let actor = u::action::Actor { empire_id: connection_state.obtain_empire_id().ok_or(anyhow!("Empire ID not available"))? };
		let mut state = connection_state.state_rc.lock().unwrap();
		let failed_actions = u::action::Action::validate_batch(actions.as_slice(), &state.universe, &actor);
		if failed_actions.is_empty() {
			state.universe.get_mut(actor.empire_id).ok_or(anyhow!("Bad empire ID stored"))?.submitted_actions = Some(actions);
			fn is_everyone_ready(empires: Vec<ui::Empire>) -> bool {
				empires.iter().all(|e| e.data.submitted_actions.is_some())
			}
			if is_everyone_ready(ui::Universe { data: &state.universe }.empires()) {
				state.make_turn()?;
			}
			Ok(cborpc::CallResponse{success:true, message: vec![]})
		} else {
			let error_messages: Vec<String> = failed_actions.iter().map(|f| f.error.clone()).collect();
			bail!(error_messages.as_slice().join("\n"))
		}
		
	}
	
	fn get_empire_id(connection_state: &mut ConnectionState, _message: &cborpc::Data) -> Result<cborpc::CallResponse> {
		let empire_id = connection_state.obtain_empire_id();
		match empire_id {
			Some(empire_id) => Ok(cborpc::CallResponse{success: true, message: rmp_serde::encode::to_vec(&empire_id)?}),
			None => Ok(cborpc::CallResponse{success: false, message: rmp_serde::encode::to_vec(&"All empires taken")?}),
		}
	}
	
	fn get_view(connection_state: &mut ConnectionState, _message: &cborpc::Data) -> Result<cborpc::CallResponse> {
		let empire_id = connection_state.obtain_empire_id().ok_or(anyhow!("Empire ID not available"))?;
		let state = connection_state.state_rc.lock().unwrap();
		let u_view = uv::Universe::empire_view(&state.universe, empire_id);
		Ok(cborpc::CallResponse{success: true, message: rmp_serde::encode::to_vec(&u_view)?})
	};

	fn get_turn(connection_state: &mut ConnectionState, _message: &cborpc::Data) -> Result<cborpc::CallResponse> {
		let state = connection_state.state_rc.lock().unwrap();
		Ok(cborpc::CallResponse{success: true, message: rmp_serde::encode::to_vec(&state.universe.turn_number)?})
	}
	
	let mut responder = cborpc::Responder::new(connection_state);
	let protocols = responder.protocols();
	let mut server_protocol = hash_map::HashMap::new();
	server_protocol.insert("get_view".to_string(), get_view as cborpc::CallHandler<ConnectionState>);
	server_protocol.insert("get_turn".to_string(), get_turn as cborpc::CallHandler<ConnectionState>);
	server_protocol.insert("get_empire_id".to_string(), get_empire_id as cborpc::CallHandler<ConnectionState>);
	server_protocol.insert("get_actions".to_string(), get_actions as cborpc::CallHandler<ConnectionState>);
	server_protocol.insert("set_actions".to_string(), set_actions as cborpc::CallHandler<ConnectionState>);
	server_protocol.insert("cancel_actions".to_string(), cancel_actions as cborpc::CallHandler<ConnectionState>);
	protocols.insert("rustorion-server-0".to_string(), server_protocol);
	responder
}

async fn handle_subscriber(mut tls_connection: TlsConnection) -> Result<()> {
	loop {
		let turn_number = tls_connection.connection_state.turn_receiver.recv().await.ok_or(anyhow!("Turn sender got dropped"))?;
		let mcall = rmp_serde::encode::to_vec(&cborpc::MethodCall{ protocol_name: "rustorion-client-0".to_string(), method_name: "turn_changed".to_string(), message: rmp_serde::encode::to_vec(&turn_number)?}).unwrap().into();
		tls_connection.writer.send(mcall).await?;
		
		let response = tls_connection.reader.next().await.ok_or(anyhow!("Unexpected end of framed stream"))??;
		let response_bytes: &[u8] = response.as_ref();
		let response: cborpc::CallResponse = rmp_serde::decode::from_read(response_bytes)?;
		if !response.success {
			bail!("turn_changed call failed?!");
		}
	}
}

async fn handle_server(mut tls_connection: TlsConnection) -> Result<()> {
	let mut responder = server_responder(tls_connection.connection_state);
	let mut next_opt = tls_connection.reader.next().await;
	while next_opt.is_some() {
		let next = next_opt.ok_or(anyhow!("This will never happen"))??;
		let inpacket: &[u8] = next.as_ref();
		let mut outpacket = vec![];
		let success: bool = responder.answer_call(inpacket, &mut outpacket)?;
		
		tls_connection.writer.send(outpacket.into()).await?;
		eprintln!("Call answered success={}", success);
		next_opt = tls_connection.reader.next().await;
	}
	Ok(())
}

async fn handle_subscriber_connection(acceptor: tokio_rustls::TlsAcceptor, stream: tokio::net::TcpStream, state_rc: Arc<Mutex<State>>) -> Result<()> {
	let stream = acceptor.accept(stream).await?;
	let certv = stream.get_ref().1.get_peer_certificates().unwrap();
	let cert_vec = certv[0].0.clone();
	let cert_hash = blake3::hash(&cert_vec);
	eprintln!("Cert incoming: {:?}", cert_hash);
	let (recv, send) = tokio::io::split(stream);
	let reader = tokio_util::codec::FramedRead::new(recv, tokio_util::codec::LengthDelimitedCodec::new());
	let writer = tokio_util::codec::FramedWrite::new(send, tokio_util::codec::LengthDelimitedCodec::new());
	let mut turn_receiver = state_rc.lock().unwrap().turn_channel.1.clone();
	// Discard initial value
	turn_receiver.recv().await.ok_or(anyhow!("Something stirs in the void"))?;
	let tls_connection = TlsConnection { reader, writer, connection_state: ConnectionState { cert_hash, state_rc: state_rc.clone(), turn_receiver} };
	
	handle_subscriber(tls_connection).await
}

async fn handle_connection(acceptor: tokio_rustls::TlsAcceptor, stream: tokio::net::TcpStream, state_rc: Arc<Mutex<State>>) -> Result<()> {
	let stream = acceptor.accept(stream).await.context("Failed to accept a stream")?;
	let certv = stream.get_ref().1.get_peer_certificates().unwrap();
	let cert_vec = certv[0].0.clone();
	let cert_hash = blake3::hash(&cert_vec);
	eprintln!("Cert incoming: {:?}", cert_hash);
	let (recv, send) = tokio::io::split(stream);
	let reader = tokio_util::codec::FramedRead::new(recv, tokio_util::codec::LengthDelimitedCodec::new());
	let writer = tokio_util::codec::FramedWrite::new(send, tokio_util::codec::LengthDelimitedCodec::new());
	let mut turn_receiver = state_rc.lock().unwrap().turn_channel.1.clone();
	// Discard initial value
	turn_receiver.recv().await.ok_or(anyhow!("Something stirs in the void"))?;
	let tls_connection = TlsConnection { reader, writer, connection_state: ConnectionState { cert_hash, state_rc: state_rc.clone(), turn_receiver} };
	
	handle_server(tls_connection).await
}

async fn accept_connections(addr: impl tokio::net::ToSocketAddrs, acceptor: tokio_rustls::TlsAcceptor, handle: runtime::Handle, state_rc: Arc<Mutex<State>>) -> Result<()> {
	let mut listener = TcpListener::bind(&addr).await?;

	loop {
		let (stream, _peer_addr) = listener.accept().await?;
		let acceptor = acceptor.clone();

		let fut = handle_connection(acceptor, stream, state_rc.clone());

		handle.spawn(fut.unwrap_or_else(|err| eprintln!("{:?}", err)));
	}
}

async fn accept_subscriber_connections(addr: impl tokio::net::ToSocketAddrs, acceptor: tokio_rustls::TlsAcceptor, handle: runtime::Handle, state_rc: Arc<Mutex<State>>) -> Result<()> {
	let mut listener = TcpListener::bind(&addr).await?;

	loop {
		let (stream, _peer_addr) = listener.accept().await?;
		let acceptor = acceptor.clone();

		let fut = handle_subscriber_connection(acceptor, stream, state_rc.clone());

		handle.spawn(fut.unwrap_or_else(|err| eprintln!("{:?}", err)));
	}
}

fn main() -> Result<()> {
	let options = Options::from_args();

	let addr = options.addr.to_socket_addrs()?
		.next().ok_or(anyhow!("Failed to parse listen host"))?;
		
	let subscribe_addr = options.subscribe_addr.to_socket_addrs()?
		.next().ok_or(anyhow!("Failed to parse listen host"))?;
			
	let cert_path = std::path::Path::new(&options.cert);
	let key_path = std::path::Path::new(&options.key);
	let (cert, key) = match fs::read(&cert_path).and_then(|x| Ok((x, fs::read(&key_path)?))) {
		Ok(x) => x,
		Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
			eprintln!("generating self-signed certificate");
			let mut params = rcgen::CertificateParams::new(vec!["localhost".into()]);
			params.alg = &rcgen::PKCS_ED25519;
			let cert = rcgen::Certificate::from_params(params).unwrap();
			let key = cert.serialize_private_key_der();
			let cert = cert.serialize_der().unwrap();
			fs::write(&cert_path, &cert).context("failed to write certificate")?;
			fs::write(&key_path, &key).context("failed to write private key")?;
			(cert, key)
		}
		Err(e) => {
			bail!("failed to read certificate: {}", e);
		}
	};
    
	let mut u_file = std::fs::File::open(&options.universe).unwrap();
	let universe = rmp_serde::decode::from_read(&mut u_file)?;
	let state = State {
		universe,
		player_map: bimap::BiHashMap::<blake3::Hash, s::ID<u::Empire>>::new(),
		turn_channel: tokio::sync::watch::channel(0),
	};
	let state_rc = Arc::new(Mutex::new(state));


	let mut runtime = runtime::Builder::new()
		.threaded_scheduler()
		.enable_io()
		.build()?;
	let handle = runtime.handle().clone();
	let mut config = rustls::ServerConfig::new(Arc::new(CustomVerifier{}));
	config.set_single_cert(vec![rustls::Certificate(cert)], rustls::PrivateKey(key))
		.map_err(|err| io::Error::new(io::ErrorKind::InvalidInput, err))?;
	let acceptor = tokio_rustls::TlsAcceptor::from(Arc::new(config));

	let server_acceptor = runtime.spawn(accept_connections(addr, acceptor.clone(), handle.clone(), state_rc.clone()));
	
	let subscribe_acceptor = runtime.spawn(accept_subscriber_connections(subscribe_addr, acceptor, handle, state_rc));
	
	runtime.block_on(server_acceptor)??;
	runtime.block_on(subscribe_acceptor)??;
	
	Ok(())
}
